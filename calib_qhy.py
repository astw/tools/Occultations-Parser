import time
import math
from   System.IO import Directory

directory       = Directory.GetCurrentDirectory()
search_loop     = 20  # Number of fine search
power_threshold = 4	  # (10**power_threshold) digit
slope           = 1.4 # value between 1.2 and 1.5
Time_List       = [SharpCap.SelectedCamera.Controls[17].Value] # [40] # [ms] Wert der Aufnahmezeit wird übernommen

SharpCap.SelectedCamera.Controls.Exposure.ExposureMs = Time_List[0]
Cal_Start_Pos = SharpCap.SelectedCamera.Controls[20].Value
Cal_End_Pos   = SharpCap.SelectedCamera.Controls[19].Value
Colour_Space  = SharpCap.SelectedCamera.Controls.ColourSpace.Value
Resolution    = SharpCap.SelectedCamera.Controls[2].Value
Height        = Resolution.split("x")[1]
H             = float(Height)
usb           = SharpCap.SelectedCamera.Controls[10].Value
LED_status    = SharpCap.SelectedCamera.Controls[21].Value
cloneRect     = SharpCap.Transforms.SelectionRect

if (LED_status == "Off"):
    SharpCap.SelectedCamera.Controls[21].Value = "On"

print("Date:",  time.ctime())
print(SharpCap.SelectedCamera.Controls[18])
print("GPS Calibration LED on/off  : %s") % (SharpCap.SelectedCamera.Controls[21].Value)
print("GPS Freq Stabilization      : %s") % (SharpCap.SelectedCamera.Controls[22].Value)
print("GPS                         : %s") % (SharpCap.SelectedCamera.Controls[23].Value)
print("Rectangle Selection         : "), cloneRect
print()

def framehandler(sender, args):
    if (dumpdata):
        global Mean
        cutout = args.Frame.CutROI(SharpCap.Transforms.SelectionRect)
        Stat = cutout.GetStats()
        Mean = Stat.Item1
        cutout.Release()

def evthandler(sender, args):
    if (SharpCap.SelectedCamera != None):
        SharpCap.SelectedCamera.FrameCaptured -= framehandler

def monitorFrames():
    SharpCap.SelectedCamera.FrameCaptured += framehandler

def Search_next_L(M, a):
    k = 0

    while (M[k][1] < a):
        k = k + 1

    return(math.floor((M[k][0] + M[k-1][0]) / 2.0))

def Search_next_R(M, a):
    k = 0

    while (M[k][1] > a):
        k = k + 1

    return(math.floor((M[k][0] + M[k-1][0]) / 2.0))

def LED_Start_search(Pos, LED, n):
    P = Pos
    L = LED

    for i in range(0,n):
        average = (min(L) + max(L)) / 2.0
        M = zip(P,L)
        M.sort()
        Pnew = Search_next_L(M,average)
        SharpCap.SelectedCamera.Controls[20].Value = int(Pnew)
        time.sleep(wait_time)
        P.append(Pnew)
        L.append(Mean)

        if (abs((L[-1] - average)/average) < 0.05):
            break

    M = zip(P,L)
    M.sort()

    return(M)

def LED_End_search(Pos, LED, n):
    P = Pos
    L = LED

    for i in range(0,n):
        average = (min(L) + max(L)) / 2.0
        M = zip(P,L)
        M.sort()
        Pnew = Search_next_R(M,average)
        SharpCap.SelectedCamera.Controls[19].Value = int(Pnew)
        time.sleep(wait_time)
        P.append(Pnew)
        L.append(Mean)

        if (abs((L[-1] - average)/average) < 0.05):
            break

    M = zip(P,L)
    M.sort()

    return(M)

# LED Calibration Start Position Adjustment
def LED_Start(expos_ms):
    Cal_Start_Pos = SharpCap.SelectedCamera.Controls[20].Value

    if (Colour_Space == "MONO16"):
        cal_step = math.floor(math.log10(Cal_Start_Pos)) - 1

    if (Colour_Space == "MONO8"):
        cal_step = math.floor(math.log10(Cal_Start_Pos)) - 1

    Pos = [Cal_Start_Pos]

    if (cal_step > power_threshold):
        cal_step = power_threshold

    print()
    print("Digit position selection:  "),

    for p in range(int(cal_step), 0, -1):
        print 10**p,
        Pos.append(Cal_Start_Pos + 10**p)
        Pos.append(Cal_Start_Pos - 10**p)

    Pos.sort()
    print()
    print("Cal_Start_Pos list: "), Pos

    LED  = []
    loop = 1000

    for L in range(0,len(Pos)):
        SharpCap.SelectedCamera.Controls[20].Value = int(Pos[L])
        time.sleep(wait_time)
        LED.append(Mean)

        if (L==0):
            print("%i   %8.3f") % (int(Pos[L]), LED[L])
        else:
            print("%i   %8.3f") % (int(Pos[L]), LED[L])
            average = (LED[L]+LED[L-1])/2.0
            diff = LED[L]-LED[L-1]

            if (abs(diff / average) > slope):
                loop = L

        if (loop == (L - 2)):
            print("Break")
            break

    print("Min / Max  : %6.3f   %6.3f") % (min(LED), max(LED))
    print("Half height: %6.3f") % ((min(LED) + max(LED)) / 2.0)
    Pos = Pos[0 : len(LED)]
    M = zip(Pos, LED)
    M.sort()
    time.sleep(wait_time)
    Result = LED_Start_search(Pos, LED, search_loop)
    Pos, LED = zip(*Result)
    print ()
    mitte = (min(LED) + max(LED)) / 2.0
    l_min = mitte - (mitte - min(LED))*0.9
    l_max = mitte + (max(LED) - mitte)*0.9
    Pos_m = []
    LED_m = []
    Y_m = []

    for i in range(0,len(Pos)):

        if (LED[i]>l_min) & (LED[i]<l_max):
            Pos_m.append(Pos[i])
            LED_m.append(LED[i])
            Y_m.append(-(math.log((max(LED) - min(LED)) / (LED[i] - min(LED)) - 1.0)))
            print("%i %10.4f") % (Pos_m[-1], LED_m[-1])

    if (len(Pos_m) < 2):
        print("Direct value:")
        print("%i %10.4f") % (Pos_m[-1], LED_m[-1])
        return(Pos_m[-1])

    print()

    if (len(Pos_m) > 1.5):
        Sx2 = 0
        Sx = 0
        Vx2 = []
        Vx = []
        n = len(Pos_m)

        for i in range(0,len(Pos_m)):
            xi = Pos_m[i] - Pos_m[0]
            Vx.append(xi)
            Vx2.append(xi**2)
            Sx = Sx + xi
            Sx2 = Sx2 + xi**2

        Det = float(Sx2 * n - Sx * Sx)
        Inv = [[n / Det,-Sx / Det],[-Sx / Det,Sx2 / Det]]

        a = 0
        b = 0

        for i in range(0,len(Pos_m)):
            a = a + (Inv[0][0]*Vx2[i] + Inv[0][1]) * LED_m[i]
            b = b + (Inv[1][0]*Vx2[i] + Inv[1][1]) * LED_m[i]

        k = a
        m = -b / a
        Cal_Start_Pos = round(Pos_m[0] + m)
        print("Optimisation: n = %i, k = %10.6f, m = %10.6f, Cal Start Pos = %i") % (n,k,m,Cal_Start_Pos)
        SharpCap.SelectedCamera.Controls[20].Value = int(Cal_Start_Pos)

        return(Cal_Start_Pos)

# LED Calibration End Position Adjustment
def LED_End(expos_ms):
    Cal_End_Pos = SharpCap.SelectedCamera.Controls[19].Value
    SharpCap.SelectedCamera.Controls[19].Value = Cal_End_Pos + 1
    SharpCap.SelectedCamera.Controls[19].Value = Cal_End_Pos - 1
    cal_step = math.floor(math.log10(Cal_End_Pos)) - 1
    Pos = [Cal_End_Pos]

    if (cal_step > power_threshold):
        cal_step = power_threshold

    print()
    print("Digit position selection:  "),

    for p in range(int(cal_step), 0, -1):
        print 10**p,
        Pos.append(Cal_End_Pos + 10**p)
        Pos.append(Cal_End_Pos - 10**p)

    Pos.sort()
    print()
    print("Cal_End_Pos list: "), Pos

    LED = []
    loop = 1000

    for L in range(0,len(Pos)):
        SharpCap.SelectedCamera.Controls[19].Value = int(Pos[L])

        time.sleep(wait_time)
        LED.append(Mean)

        if (L==0):
            print("%i   %8.3f") % (Pos[L], LED[L])
        else:
            print("%i   %8.3f") % (Pos[L], LED[L])
            average = (LED[L]+LED[L-1])/2.0
            diff = LED[L]-LED[L-1]
            if (abs(diff / average) > slope):
                loop = L

        if (loop == (L - 2)):
            print "Break"
            break

    print("Min / Max  : %6.3f   %6.3f") % (min(LED), max(LED))
    print("Half height: %6.3f") % ((min(LED) + max(LED)) / 2.0)
    Pos = Pos[0 : len(LED)]
    M = zip(Pos, LED)
    M.sort()
    time.sleep(wait_time)
    Result = LED_End_search(Pos, LED, search_loop)
    Pos, LED = zip(*Result)
    print()
    mitte = (min(LED) + max(LED)) / 2.0
    l_min = mitte - (mitte - min(LED))*0.9
    l_max = mitte + (max(LED) - mitte)*0.9
    Pos_m = []
    LED_m = []
    Y_m = []

    for i in range(0,len(Pos)):

        if (LED[i]>l_min) & (LED[i]<l_max):
            Pos_m.append(Pos[i])
            LED_m.append(LED[i])
            Y_m.append((math.log((max(LED) - min(LED)) / (LED[i] - min(LED)) - 1.0)))
            print("%i %10.6f") % (Pos_m[-1], LED_m[-1])

    print()

    if (len(Pos_m) < 2):
        print("Direct value:")
        print("%i %10.4f") % (Pos_m[-1], LED_m[-1])
        return(Pos_m[-1])

    if (len(Pos_m) > 1.5):
        Sx2 = 0
        Sx = 0
        Vx2 = []
        Vx = []
        n = len(Pos_m)

        for i in range(0,len(Pos_m)):
            xi = Pos_m[i] - Pos_m[0]
            Vx.append(xi)
            Vx2.append(xi**2)
            Sx = Sx + xi
            Sx2 = Sx2 + xi**2

        Det = float(Sx2 * n - Sx * Sx)
        Inv = [[n / Det,-Sx / Det],[-Sx / Det,Sx2 / Det]]

        a = 0
        b = 0

        for i in range(0,len(Pos_m)):
            a = a + (Inv[0][0]*Vx2[i] + Inv[0][1]) * LED_m[i]
            b = b + (Inv[1][0]*Vx2[i] + Inv[1][1]) * LED_m[i]

        k = a
        m = -b / a
        Cal_End_Pos = round(Pos_m[0] + m)
        print("Optimisation: n = %i, k = %10.6f, m = %10.6f, Cal End Pos = %i") % (n,k,m,Cal_End_Pos)
        SharpCap.SelectedCamera.Controls[19].Value = int(Cal_End_Pos)

        return(Cal_End_Pos)

print()
print "Time sequence  [ms] :", Time_List
Cal_Start = []
Cal_End = []
SC_Start = []
SC_End = []
time_start = time.clock()
dumpdata = True
SharpCap.CaptureEvent += evthandler
monitorFrames()

for T in range(0,len(Time_List)):
    SharpCap.SelectedCamera.Controls.Exposure.ExposureMs = Time_List[T]
    expos_ms = SharpCap.SelectedCamera.Controls.Exposure.ExposureMs
    wait_time = 2.1 *(expos_ms / 1000.0) + 0.2
    Cal_End_Pos = SharpCap.SelectedCamera.Controls[19].Value
    Cal_Start_Pos = SharpCap.SelectedCamera.Controls[20].Value

    if (Colour_Space == "MONO16"):
        Time_limit = (usb + 9.76) * (0.001067 * H + 0.0435)

        if (Time_limit > expos_ms):
            StartPos = int(((0.0000390615*H - 0.0707123)*H - 74967.6652)*expos_ms + ((-0.0035966154*H + 86.4884205)*H + 1635.68176)*usb + (-0.0344038914*H + 842.6668063)*H + 16080.628624)
            EndPos = int(4190 + 320*usb)

        if (Time_limit <= expos_ms):
            StartPos = int(1120 * usb + 10939)
            EndPos = int(4190 + 320*usb)

    if (Colour_Space == "MONO8"):
        Time_limit = (usb + 5.60) * (0.001067 * H + 0.0421)

        if (Time_limit > expos_ms):
            StartPos = int(((0.0000637116*H - 0.1436327)*H - 74917.10660)*expos_ms + ((-0.00358619*H+86.459717)*H + 1607.70533)*usb + (-0.019717898*H+483.579932)*H + 9199.23345)
            EndPos = int(2850 + 320*usb)

        if (Time_limit <= expos_ms):
            StartPos = int(1120 * usb + 6276)
            EndPos = int(2850 + 320*usb)

    Cal_Start_Pos = StartPos
    Cal_End_Pos = EndPos
    SharpCap.SelectedCamera.Controls[19].Value = int(Cal_End_Pos)
    SharpCap.SelectedCamera.Controls[20].Value = int(Cal_Start_Pos)
    SC_End.append(int(Cal_End_Pos))
    SC_Start.append(int(Cal_Start_Pos))
    print()
    print()
    print "Exposure Time: ", Time_List[T], " ms"
    print("=================================")
    Cal_Start.append(int(LED_Start(Time_List[T])))
    Cal_End.append(int(LED_End(Time_List[T])))
    SharpCap.SelectedCamera.Controls[19].Value = Cal_End[-1]
    SharpCap.SelectedCamera.Controls[20].Value = Cal_Start[-1]

time_end = time.clock()
date = time.localtime()
file_date = time.strftime("_%Y%m%d_%H%M%S",date)

file_name = 'C:\\Users\\User\\Documents\\LED_'

file_name = file_name + "_" + str(SharpCap.SelectedCamera.Controls.ColourSpace.Value)
file_name = file_name + "_" + str(SharpCap.SelectedCamera.Controls.Resolution.Value)
file_name = file_name + "_" + str(SharpCap.SelectedCamera.Controls.Binning.Value)
file_name = file_name + "_USB" + str(SharpCap.SelectedCamera.Controls.Usb.Value)
file_name = file_name + file_date + '_.txt'

print()
print()

with open(file_name, "w") as myfile:
    print("Camera Settings:")
    myfile.writelines("Camera Settings:" + "\n")
    print("========================================================")
    print("Colour Space : "), SharpCap.SelectedCamera.Controls.ColourSpace.Value
    myfile.writelines(("Colour Space :, %s \n") % (str(SharpCap.SelectedCamera.Controls.ColourSpace.Value)))
    print("Binning      : "), SharpCap.SelectedCamera.Controls.Binning.Value
    myfile.writelines(("Binning      :, %s \n") % (str(SharpCap.SelectedCamera.Controls.Binning.Value)))
    print("USB Traffic  : "), SharpCap.SelectedCamera.Controls.Usb.Value
    myfile.writelines(("USB Traffic  :, %s \n") % (str(SharpCap.SelectedCamera.Controls.Usb.Value)))
    print()
    print "Calculation time: ", round((time_end - time_start)/60, 2), " minutes"
    myfile.writelines(("Calculation time:, %s \n") % (str(round((time_end - time_start)/60, 2))))
    print()
    print(" [ms]  Cal_Start    Cal_End       Exp.   LED-time    Delta   SC_Start     SC_End ")
    myfile.writelines("[ms],Cal_Start,Cal_End,Exp.,LED-time,Delta,SC_Start,SC_End" + "\n")
    print("================================================================================")

    for T in range(0,len(Time_List)):
        LED_time = (Cal_End[T] - Cal_Start[T]) / 75000.0
        Diff_time = Time_List[T] - LED_time
        print("%5s %10s %10s %10.2f %10.3f %8.3f") % (Time_List[T],Cal_Start[T],Cal_End[T],Time_List[T],LED_time,Diff_time),
        print("%10s %10s") % (SC_Start[T],SC_End[T])
        myfile.writelines(("%5s,%10s,%10s,%10.2f,%10.3f,%8.3f,%10s,%10s") % (Time_List[T],Cal_Start[T],Cal_End[T],Time_List[T],LED_time,Diff_time,SC_Start[T],SC_End[T]) + "\n")

dumpdata = False
myfile.close()
Cal_Start_Pos = SharpCap.SelectedCamera.Controls[20].Value
SharpCap.SelectedCamera.Controls[20].Value = int(Cal_Start_Pos)

if (LED_status == "On"):
    SharpCap.SelectedCamera.Controls[21].Value = "Off"

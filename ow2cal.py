import sys
import pytz
import locale
import astropy.units       as     u
from   datetime            import datetime, timedelta
from   icalendar           import Calendar, Event


locale.setlocale(locale.LC_TIME, "de_DE")
berlin_timezone = pytz.timezone('Europe/Berlin')
owexport        = []
mydates         = []
days            = 5

if len(sys.argv) > 1:
    days = int(sys.argv[1])

duration_days = datetime.now() + timedelta(days = days)

with open("OWExport.csv", "r") as f:
    owexport = f.readlines()

for l in owexport:

    if len(l.strip()) == 0:
        continue

    if l.startswith("Alle"):
        continue

    if l.startswith("Mein"):
        continue

    if l.startswith("\"Name"):
        continue

    el                = l.split("\"")
    name              = el[1].strip()
    date_string       = el[3].strip()
    date_object       = datetime.strptime("2024 "+ date_string, "%Y %a %d %b, %H:%M")
    real_date_object  = datetime(datetime.now().year, date_object.month, date_object.day, date_object.hour, date_object.minute, date_object.second)
    aware_date_object = berlin_timezone.localize(real_date_object)
    mag               = float(el[5].strip().replace(",","."))
    duration          = float(el[11].replace(',','.'))

    if aware_date_object > berlin_timezone.localize(datetime.now()) and aware_date_object < berlin_timezone.localize(duration_days):
        mydates.append([name, mag, real_date_object, duration, l])

cal = Calendar()
cal.add('prodid',  'OcultWatcher Exporter')
cal.add('version', '0.00987')

for i in mydates:
    event = Event()
    event.add('summary', f"{i[0]}")
    event.add('dtstart', i[2])
    event.add('dtend', i[2] + timedelta(minutes=5))
    event.add('dtstamp', datetime.now(pytz.utc))
    event.add('location', 'Archenhold Observatory, Alt-Treptow 1, 12435 Berlin, Germany')
    event.add('description', f"Magnitude: {i[1]}\nDuration: {i[3]}")
    event['uid'] = i[2].timestamp()
    cal.add_component(event)

with open('OWExport.ics', 'wb') as f:
    f.write(cal.to_ical())

import sys
import astropy.units       as     u
from   astropy.time        import Time
from   astropy.coordinates import SkyCoord, EarthLocation, Angle, AltAz
from   art                 import *
from   xml.dom             import minidom

latitude   = 52.4865802
longitude  = 13.4752174
elevation  = 100

dom           = minidom.parse(sys.argv[1])
ocevent       = dom.getElementsByTagName('Occultations')[0]
star          = ocevent.getElementsByTagName('Star')[0].firstChild.data.split(",")
name          = star[0]

ra            = float(star[1])
dec           = float(star[2])
star_coord    = SkyCoord(ra, dec, unit = u.deg)

obs_time      = Time.now()
lst           = obs_time.sidereal_time('mean', longitude)
ha            = Angle(lst.value - star_coord.ra.value, unit=u.hourangle).wrap_at(24 * u.hourangle)

dec_formatted = star_coord.dec.to_string(unit = u.degree, sep = ' ', pad = True, precision = 2)
ra_formatted  = star_coord.ra.to_string (unit = u.degree, sep = ' ', pad = True, precision = 2)
ha_formatted  = ha.to_string(                             sep = ' ', pad = True, precision = 2)

location      = EarthLocation(lat = latitude * u.degree, lon = longitude * u.degree, height = elevation * u.meter)
altaz_frame   = AltAz(obstime = obs_time, location = location)
obj_altaz     = star_coord.transform_to(altaz_frame)
alt           = obj_altaz.alt
az            = obj_altaz.az
dir_names     = ["North", "NorthEast", "East", "SouthEast", "South", "SouthWest", "West", "NorthWest", "North"]
az_name       = dir_names[round(az.value / 45)]

print(text2art(f"Name-> {name}",          font = 'utopia'))
print(text2art(f"Dec -> {dec_formatted}", font = 'utopia'))
print(text2art(f"Stw -> {ha_formatted}",  font = 'utopia'))
print(f"Sharpcap =====================> {ra_formatted}, {dec_formatted} <===================== ")
print(f"ALT -> {alt:.1f}")
print(f"AZ  -> {az_name} ({az:.1f})")

# https://cloud.occultwatcher.net/event/1161-4194-90402-649096-U026013/1455113
